"""happyweb URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, re_path
from app import views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('admin/', admin.site.urls),
    path('persondataview/', views.PersonalView.as_view()),
    path('whatisfor/', views.WhatsForView.as_view()),
    path('worksfor/', views.WorkoptionView.as_view()),
    path('hearFrom/', views.HearaboutView.as_view()),
    re_path('updateperson/(?P<pk>\d+)/', views.UpdatePersonView.as_view()),
    # url('fileupload/',views.FileUploadView.as_view()),
    # path('api/personapiview/', views.PersonalView.as_view()),
    path('idproofview/',views.IdProofView.as_view()),
    path('income_type/',views.IncomeTypeView.as_view()),

    path('mailsend/',views.sendmailandcreatepdf),
    path('contact_no/',views.ContactNo),
    
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)+ static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

admin.site.site_header = ("")
admin.site.site_title = ("Happy Loan Admin")
