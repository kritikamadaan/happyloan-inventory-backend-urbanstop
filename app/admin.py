from django.contrib import admin
# from.models import PersonDetail,WhatIsItFor,WorkOption,HearAboutUs,IdProofType
from .models import *
from import_export.admin import ExportActionModelAdmin
# Register your models here.
@admin.register(WhatIsItFor)
class WhatsForAdmin(admin.ModelAdmin):
    list_display=('index','option')
    search_fields=('index','option')


#admin.site.register(PersonDetail)
admin.site.register(HearAboutUs)
admin.site.register(WorkOption)
admin.site.register(IdProofType)
admin.site.register(IncomeDocType)
# admin.site.register(Configuration)
@admin.register(Configuration)
class ConfigureAdmin(admin.ModelAdmin):
    list_display=('emails','phone')


@admin.register(PersonDetail)
class Personview(ExportActionModelAdmin,admin.ModelAdmin):
    list_display=('first_name','last_name','email_id','ph_no','ownerhome','other_property',)
    search_fields=('first_name','last_name','email_id','ph_no','state','city')
    list_filter=('are_you_married','ownerhome','other_property','state','city')
