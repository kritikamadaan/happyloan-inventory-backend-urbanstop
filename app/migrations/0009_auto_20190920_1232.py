# Generated by Django 2.2.4 on 2019-09-20 12:32

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0008_auto_20190920_1231'),
    ]

    operations = [
        migrations.AlterField(
            model_name='persondetail',
            name='id_proof_pdf_file',
            field=models.FilePathField(blank=True, default='', path='/home/sushanta/sushanta/work/HAPPYLOAN/happyloan-service/media'),
        ),
    ]
