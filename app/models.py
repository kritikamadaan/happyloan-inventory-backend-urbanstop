from django.db import models
from django.conf import settings

class Configuration(models.Model):
    emails=models.CharField(max_length=200,help_text="You can give multiple emails seperated by comma")
    phone=models.CharField(max_length=15,verbose_name="Contact No")

    def __str__(self):
        return str(self.emails)
    
    # def arranged_phone(self):
    #     if '-' not in str(self.phone):
    #         a2=str(self.phone)
    #         t=""
    #         for i in range(1,4) and a2 is not None:
    #             if i==1:
    #                 t+='-'+a2[:1]
    #                 a2=a2[1:]
    #             if i>1 and i<=3:
                    
    #                 t+='-'+a2[:3]
    #                 a2=a2[3:]
    #             else
    #                 t+='-'+a2[:4]
    #         print('updated phone no=',t)
    #         return t
    #     else:
    #         return self.phone

class WhatIsItFor(models.Model):
    index=models.PositiveIntegerField()
    option = models.CharField(max_length=100)

    class Meta:
        verbose_name='What Is It For'
        verbose_name_plural='What Is It For'

    def __str__(self):
        return self.option

class IdProofType(models.Model):
    option=models.CharField(max_length=100)

    class Meta:
        verbose_name='Id Proof Type'
        verbose_name_plural='Id Proof Type'      
    def __str__(self):
        return self.option      

class IncomeDocType(models.Model):
    option=models.CharField(max_length=100)

    class Meta:
        verbose_name='Id Proof Type of Income'
        verbose_name_plural='Id Proof Type of Income'   

    def __str__(self):
        return self.option      



class HearAboutUs(models.Model):
    option = models.CharField(max_length=100)

    class Meta:
        verbose_name='Hear About Us'
        verbose_name_plural='Hear About Us'


    def __str__(self):
        return self.option


class WorkOption(models.Model):
    option = models.CharField(max_length=100)

    class Meta:
        verbose_name=' Work Type'
        verbose_name_plural='Work Type'

    def __str__(self):
        return self.option


class PersonDetail(models.Model):
    money = models.BigIntegerField()
    whatsfor = models.ForeignKey(WhatIsItFor, on_delete=models.CASCADE)
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100,null=True)
    ph_no = models.CharField(max_length=30)
    email_id = models.CharField(max_length=50)
    sin_no=models.CharField(max_length=15,null=True,blank=True)
    date_of_birth=models.DateField(null=True,blank=True)
    ownerhome = models.BooleanField()
    hear_about_us = models.ForeignKey(HearAboutUs, on_delete=models.CASCADE)
    referral = models.CharField(max_length=100, null=True, blank=True)
    #calltime = models.TimeField(null=True, blank=True)
    address_line_1 = models.TextField(null=True)
    #address_line_2 = models.TextField(null=True, blank=True)
    city = models.CharField(max_length=100,null=True, blank=True)
    state = models.CharField(max_length=100,null=True, blank=True)
    zip = models.CharField(max_length=10,null=True, blank=True)
    other_property = models.BooleanField(null=True)
    work_type = models.ForeignKey(WorkOption, on_delete=models.CASCADE, null=True)
    work_type_others=models.CharField(max_length=30,null=True,blank=True) 
    month_income = models.BigIntegerField(null=True)
    are_you_married = models.BooleanField(null=True)
    spouse_name = models.CharField(max_length=50, null=True, blank=True)
    spouse_phone = models.CharField(max_length=40, null=True, blank=True)
    spouse_email = models.CharField(max_length=100, null=True, blank=True)
    spouce_sin_no=models.CharField(max_length=15,null=True, blank=True)
    spouce_date_of_birth=models.DateField(null=True,blank=True)
    spouse_income = models.BigIntegerField(null=True, blank=True)
    comments = models.TextField(null=True, blank=True)
    id1_proof_frontend = models.ImageField(null=True)
    id1_proof_backend = models.ImageField(null=True)

    income1_proof_frontend = models.ImageField(null=True,blank=True)
    # income1_proof_backend = models.ImageField(null=True,blank=True)
    void_cheque=models.ImageField(null=True,blank=True)
    id_proof_pdf_file= models.FileField(null=True,blank=True)
    step_submitted = models.CharField(null=True,max_length=20)
    
    id1_proof_type=models.ForeignKey(IdProofType, on_delete=models.PROTECT, related_name='Id_Proof1', null=True,blank=True)
    
    id2_proof_type=models.ForeignKey(IdProofType, on_delete=models.PROTECT, related_name='Id_Proof2', null=True,blank=True)

    income1_proof_type=models.ForeignKey(IncomeDocType, on_delete=models.PROTECT, related_name='income_Id_Proof1', null=True,blank=True)
    
    income2_proof_type=models.ForeignKey(IncomeDocType, on_delete=models.PROTECT, related_name='income_Id_Proof2', null=True,blank=True)


    income2_proof_frontend = models.ImageField(null=True,blank=True)
    # income2_proof_backend = models.ImageField(null=True,blank=True)

    id2_proof_frontend = models.ImageField(null=True)
    id2_proof_backend = models.ImageField(null=True)
    terms_conditions = models.BooleanField(null=True)

    class Meta:
        verbose_name='Form Submission'
        verbose_name_plural='Form Submissions'

    def __str__(self):
        return self.first_name + ' '+self.last_name 
