from rest_framework import serializers
# from.models import PersonDetail,WhatIsItFor,WorkOption,HearAboutUs,IdProofType
from .models import *

class PersonalSerializer(serializers.ModelSerializer):
    class Meta:
        model=PersonDetail
        fields="__all__"

class WhatsForSerializer(serializers.ModelSerializer):
    class Meta:
        model=WhatIsItFor
        fields="__all__"

class WorkOptionSerializer(serializers.ModelSerializer):
    class Meta:
        model=WorkOption
        fields="__all__"

class HearAboutUsSerializer(serializers.ModelSerializer):
    class Meta:
        model=HearAboutUs
        fields="__all__"


class IdProofTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model=IdProofType
        fields="__all__"


class IncomeTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model=IncomeDocType
        fields="__all__"


# class Id2ProofTypeSerializer(serializers.ModelSerializer):
#     class Meta:
#         model=Id2ProofType
#         fields="__all__"
