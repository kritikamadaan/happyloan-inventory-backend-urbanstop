from django.shortcuts import render,HttpResponse
from rest_framework import generics,serializers
# from.models import PersonDetail,WhatIsItFor,WorkOption,HearAboutUs,IdProofType
# from.serializers import PersonalSerializer,WhatsForSerializer,WorkOptionSerializer,HearAboutUsSerializer,IdProofTypeSerializer
# from django_filters import Filter
from .models import *
from .serializers import *
from rest_framework.decorators import authentication_classes, permission_classes
from rest_framework.permissions import AllowAny
from django.views.decorators.csrf import csrf_exempt
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.parsers import JSONParser
from . import CsrfExemptSessionAuthentication
from rest_framework.authentication import SessionAuthentication, BasicAuthentication 
from django.core.mail import send_mail,EmailMessage
import json
import socket
import os
from fpdf import FPDF
from django.contrib.sites.shortcuts import get_current_site
from django.contrib.auth.models import User
from django.core.files import File
import random
import string
from multiprocessing import Pool

from django.db.models.signals import post_save,pre_save
from django.conf import settings
from django.views.decorators.csrf import csrf_exempt
from rest_framework.decorators import api_view,permission_classes
from rest_framework.views import Response
import zcrmsdk

configuration_dictionary = {
'apiBaseUrl':'https://www.zohoapis.com',
'apiVersion':'v2',
'currentUserEmail':'yes@happyloan.ca',
'sandbox':'False',
'applicationLogFilePath':'/home/ubuntu/zoholog',
'client_id':'1000.HCCAIR66SRB9RCODM6V75Q3L2MA0UZ',
'client_secret':'2798bd459782d73f59a823288f11d8a662a68e9c22',
'redirect_uri':'https://www.abc.com',
'accounts_url':'https://accounts.zoho.com',
'token_persistence_path':'/home/ubuntu/FilePersistence',
'access_type':'online',

}
zcrmsdk.ZCRMRestClient.initialize(configuration_dictionary)
def insert_lead_to_zoho(instance):
    print("Inside Insert Lead tO ZOHO")
    try:
        fname = instance.first_name
        phno=instance.ph_no
        lname = instance.last_name
        emailid=instance.email_id
        referral=instance.referral
        try:
            dob =instance.date_of_birth.strftime("%Y-%m-%d")
        except:
            dob = 'N/A'
        try:
            spouse_dob = instance.spouce_date_of_birth.strftime("%Y-%m-%d")
        except:
            spouse_dob='N/A'
        if instance.step_submitted == 'steptwo':
            comment = """
                Home owner :%s
                Referral :%s
                City :%s
                State :%s
                Other Property :%s
                Work Type :%s
                Are You Married :%s
                Spouse Income :%s
                """%(
                    # formatNA(formatCurrency(instance.money)),
                    # formatNA(instance.whatsfor),
                    formatNA(instance.ownerhome),
                    formatNA(referral),
                    # formatNA(instance.address_line_1),
                    formatNA(instance.city),
                    formatNA(instance.state),
                    # formatNA(instance.zip),
                    formatNA(instance.other_property),
                    formatNA(instance.work_type),
                    # formatNA(formatCurrency(instance.month_income)),
                    formatNA(instance.are_you_married),
                    # formatNA(instance.spouse_name),
                    # formatNA(instance.spouse_phone),
                    # formatNA(instance.spouse_email),
                    formatNA(formatCurrency(instance.spouse_income)))

        else:
            comment = """
                Home owner :%s
                Referral :%s
                City :%s
                State :%s
                Other Property :%s
                Work Type :%s
                Are You Married :%s
                Spouse Income :%s
                ID 1 Proof Type :%s
                ID 2 Proof Type :%s
                Income ID 1 Proof Type :%s
                income ID 2 Proof Type :%s
                """%(
                    # formatNA(formatCurrency(instance.money)),
                    # formatNA(instance.whatsfor),
                    formatNA(instance.ownerhome),
                    formatNA(referral),
                    # formatNA(instance.address_line_1),
                    formatNA(instance.city),
                    formatNA(instance.state),
                    # formatNA(instance.zip),
                    formatNA(instance.other_property),
                    formatNA(instance.work_type),
                    # formatNA(formatCurrency(instance.month_income)),
                    formatNA(instance.are_you_married),
                    # formatNA(instance.spouse_name),
                    # formatNA(instance.spouse_phone),
                    # formatNA(instance.spouse_email),
                    formatNA(formatCurrency(instance.spouse_income)),
                    # formatNA(instance.comments),
                    formatNA(instance.id1_proof_type),
                    formatNA(instance.id2_proof_type),
                    formatNA(instance.income1_proof_type),
                    formatNA(instance.income2_proof_type))


                
        print(comment)
        record = zcrmsdk.ZCRMRecord.get_instance('Leads')  # Module API Name
        record.set_field_value('First_Name', formatNA(instance.first_name))
        record.set_field_value('Last_Name', formatNA(instance.last_name))
        record.set_field_value('Postal_Code', formatNA(instance.zip))
        record.set_field_value('Mobile', formatNA(instance.ph_no).replace('-',''))
        if(instance.sin_no and instance.sin_no!=''):
            record.set_field_value('SIN_Number', formatNA(instance.sin_no).replace('-',''))
        if(instance.date_of_birth):
            record.set_field_value('Date_of_Birth1', dob)

        record.set_field_value('Total_amount_to_be_paid', formatNA(instance.money))
        record.set_field_value('Why_they_need_money', formatNA(instance.whatsfor.option))
        record.set_field_value('Annual_Income', formatNA(instance.month_income))
        record.set_field_value('Lead_Source', formatNA(instance.hear_about_us.option))

        record.set_field_value('Street', formatNA(instance.address_line_1)+','+formatNA(instance.city)+','+formatNA(instance.state)+','+formatNA(instance.zip))
        # record.set_field_value('Do_you_own_your_own_home', formatNA(instance.ownerhome))
        # record.set_field_value('Lead_Source', 'Form')
        record.set_field_value('Email', formatNA(instance.email_id))
        if(instance.spouse_name and instance.spouse_name!=''):
            record.set_field_value('Co_First_Name', formatNA(instance.spouse_name))
        if(instance.spouse_phone and instance.spouse_phone!=''):
            record.set_field_value('Co_Mobile', formatNA(instance.spouse_phone).replace('-',''))
        # record.set_field_value('Co_First_Name', 'Rathi')
        if(instance.spouce_sin_no and instance.spouce_sin_no!=''):
            record.set_field_value('Co_SIN', formatNA(instance.spouce_sin_no).replace('-',''))
        if(instance.spouce_date_of_birth):
            record.set_field_value('Co_Date_of_Birth', spouse_dob)

        if(instance.spouse_email and instance.spouse_email!=''):
            record.set_field_value('Co_Email', formatNA(instance.spouse_email))
        record.set_field_value('Comment', formatNA(instance.comments))
        
                     
        records=[]
        records.append(record)
        zcrmsdk.ZCRMRestClient.initialize(configuration_dictionary)
        resp = zcrmsdk.ZCRMModule.get_instance('Leads').create_records(records)
        print("Zoho replied:",resp.status_code,len(records))
        return "OK"
    except Exception as e:
        print(str(e))
        return "NOT OK"


def randomString(stringLength=12):
    """Generate a random string of fixed length """
    letters = string.ascii_letters
    return ''.join(random.choice(letters) for i in range(stringLength))
# Create your views here.


@csrf_exempt
@api_view(['GET'])
# @permission_classes([IsAuthenticated,])
def ContactNo(request):
    if request.method=='GET':
        config=Configuration.objects.all().last()
        print('contact ph no=',config.phone)
        return Response({'ph_no':config.phone})


class IncomeTypeView(generics.ListAPIView):
    queryset = IncomeDocType.objects.all()
    serializer_class = IncomeTypeSerializer


class IdProofView(generics.ListCreateAPIView):
    queryset = IdProofType.objects.all()
    serializer_class = IdProofTypeSerializer

class PersonalView(generics.ListCreateAPIView):
    queryset=PersonDetail.objects.all()
    serializer_class=PersonalSerializer


@authentication_classes([])
@permission_classes([])
class WhatsForView(generics.ListAPIView):
    queryset=WhatIsItFor.objects.all()
    serializer_class=WhatsForSerializer

    def get_queryset(self):
        queryset=WhatIsItFor.objects.all().order_by('index')
        return queryset

class WorkoptionView(generics.ListCreateAPIView):
    queryset=WorkOption.objects.all()
    serializer_class=WorkOptionSerializer
    
# @api_view(['POST'])    
# @permission_classes((AllowAny, ))
class HearaboutView(generics.ListCreateAPIView):
    permission_classes = (AllowAny,)
    queryset=HearAboutUs.objects.all()
    serializer_class=HearAboutUsSerializer    


class PersonApiView(APIView):
    data=PersonDetail.objects.all()
    serialize=PersonalSerializer(data,partial=True)


class UpdatePersonView(generics.UpdateAPIView):
    queryset = PersonDetail.objects.all()
    serializer_class = PersonalSerializer





def formatCurrency(a):
    if(a==None ):
        return ''
    if(str(a)==''):
        return ''
    try:
        l=str(a)
        a2=l[::-1]
        #a2=str(a2)
        t=""
        for i in range(len(a2)):
            if i%3==0 and i>0:
                t+=','+a2[i]
            else:
                t+=a2[i]

        aa=str(t[::-1])
        print(aa)
        return('$'+aa)
    except:
        return '$'+a

def formatNA(input):
    try:
        if(input is None):
            return 'N/A'
        if(input==''):
            return 'N/A'
        return input
    except:
        return input

def generatePdf(data1,data2):
    pdf=FPDF()
    pdf.add_page()
    pdf.image(data1,0,0,50,50)
    pdf.add_page()
    pdf.image(data2,30,30,50,50)
    pdf.output("yourfile.pdf", "F")
    print(pdf)    
        
def createpdf( instance):
    # print(kwargs)
    # print(instance.id1_proof_type)

    try:
        id1front = instance.id1_proof_frontend.path
        id1back = instance.id1_proof_backend.path
        id2front = instance.id2_proof_frontend.path
        id2back = instance.id2_proof_backend.path

        income1front = instance.income1_proof_frontend.path
        # income1back = instance.income1_proof_backend.path
        income2front = instance.income2_proof_frontend.path
        # income2back = instance.income2_proof_backend.path
        void_cheque_path=instance.void_cheque.path

    except Exception as e:
        print('error in create pdf',str(e))
        pass

    pdf=FPDF('P','mm',(210,297))
    pdf.set_font('Arial',style='', size=10)
        
    pdf.compress = False
    pdf.add_page()
    
    #Name Section
    pdf.set_font_size(25)

    pdf.cell(w=0,txt = instance.first_name+" "+instance.last_name,h=20, border = 0, ln=1, align = 'C', fill = False, link = '')
    #ID Proof Name Section

    pdf.set_font_size(12)
    try:
        pdf.cell(w=0,txt = instance.id1_proof_type.option+": Front", border = 0, h=20,ln=1, align = 'C', fill = False, link = '')
        pdf.image(x=45,name=id1front,w=120)
        pdf.cell(w=0,txt = instance.id1_proof_type.option+": Back", border = 0, h=20,ln=1, align = 'C', fill = False, link = '')
        pdf.image(x = 45,name=id1back,w=120)
    except Exception as e:
        print('error in create pdf',str(e))
        pass
    pdf.add_page()
        
        #Name Section
    pdf.set_font_size(25)

    pdf.cell(w=0,txt = instance.first_name+" "+instance.last_name,h=20, border = 0, ln=1, align = 'C', fill = False, link = '')
        #ID Proof Name Section
    pdf.set_font_size(12)

    try:    
        pdf.cell(w=0,txt = instance.id2_proof_type.option+": Front", border = 0, h=20,ln=1, align = 'C', fill = False, link = '')
        pdf.image(x=45,name=id2front,w=120)
        pdf.cell(w=0,txt = instance.id2_proof_type.option+": Back", border = 0, h=20,ln=1, align = 'C', fill = False, link = '')
        pdf.image(x = 45,name=id2back,w=120)
        pdf.add_page()
    except Exception as e:
        print('error in create pdf',str(e))
        # income Id 1 proof
    pdf.set_font_size(25)
    pdf.cell(w=0,txt = instance.first_name+" "+instance.last_name,h=20, border = 0, ln=1, align = 'C', fill = False, link = '')
    pdf.set_font_size(12)
    try:
        pdf.cell(w=0,txt = instance.income1_proof_type.option, border = 0, h=20,ln=1, align = 'C', fill = False, link = '')
        pdf.image(x=45,name=income1front,w=120)
        # Income ID 2
        pdf.cell(w=0,txt = instance.income2_proof_type.option, border = 0, h=20,ln=1, align = 'C', fill = False, link = '')
        pdf.image(x=45,name=income2front,w=120)
        # pdf.cell(w=0,txt = instance.income1_proof_type.option+": Back", border = 0, h=20,ln=1, align = 'C', fill = False, link = '')
        # pdf.image(x = 45,name=income1back,w=120)
        pdf.add_page()
    except Exception as e:
        print('error in create pdf',str(e))
        pass
        # Income ID 2

    # pdf.set_font_size(25)
    # pdf.cell(w=0,txt = instance.first_name+" "+instance.last_name,h=20, border = 0, ln=1, align = 'C', fill = False, link = '')
    # pdf.set_font_size(12)
    # try:
    #     pdf.cell(w=0,txt = instance.income2_proof_type.option, border = 0, h=20,ln=1, align = 'C', fill = False, link = '')
    #     pdf.image(x=45,name=income2front,w=120)
    #     # pdf.cell(w=0,txt = instance.income2_proof_type.option+": Back", border = 0, h=20,ln=1, align = 'C', fill = False, link = '')
    #     # pdf.image(x = 45,name=income2back,w=120)
    #     pdf.add_page()
    # except Exception as e:
    #     print('error in create pdf',str(e))
    #     pass

    pdf.set_font_size(25)
    pdf.cell(w=0,txt = instance.first_name+" "+instance.last_name,h=20, border = 0, ln=1, align = 'C', fill = False, link = '')
    pdf.set_font_size(12)
    
    try:
        pdf.cell(w=0,txt =" Void Cheque", border = 0, h=20,ln=1, align = 'C', fill = False, link = '')
        pdf.image(x=45,name=void_cheque_path,w=120)
    except Exception as e:
        print('error in create pdf',str(e))
        pass    

    filename = instance.first_name + randomString()+'.pdf'
    pdf.output('media/'+filename, 'F')
        # f = open(os.path.join(settings.MEDIA_ROOT,filename),encoding='latin-1')

        # pdf_cont = pdf.output(dest='S').encode('latin-1')
    instance.id_proof_pdf_file.name=filename
    instance.save()
    # except Exception as e:
    #     print('error in create pdf',str(e))


def sendmail_for_step2(instance,to_email):
    try:
        print('call for step 2')

        fname = instance.first_name.title()
        lname = instance.last_name.title()
        sub="New LEAD- "+ lname+', '+ fname+'(Updated for second step)'

        data = """<html><body>
            <b style="font-size:14px">New lead:%s, %s has completed step two</b>
            <table>
            <tr>
            <td>First Name :</td><td>%s</td>
            </tr>
            <tr><td>Last Name :</td><td>%s</td></tr>
            <tr><td>Phone No :</td><td>%s</td></tr>
            <tr><td>Email Id :</td><td>%s</td></tr>
            <tr><td>Money :</td><td>%s</td></tr>
            <tr><td>What is it for :</td><td>%s</td></tr>
            <tr><td>Home owner :</td><td>%s</td></tr>
            <tr><td>Hear About Us :</td><td>%s</td></tr>
            <tr><td>Referral :</td><td>%s</td></tr>
            <td>Address :</td><td>%s</td>
            </tr>
            <tr><td>DOB :</td><td>%s</td></tr>
            <tr><td>Monthly Income :</td><td>%s</td></tr>
            <tr><td>SIN No :</td><td>%s</td></tr>
            <tr><td>City :</td><td>%s</td></tr>
            <tr><td>State :</td><td>%s</td></tr>
            <tr><td>zip :</td><td>%s</td></tr>
            <tr><td>Other Property :</td><td>%s</td></tr>
            <tr><td>Income Type :</td><td>%s</td></tr>
            <tr><td>Is Married :</td><td>%s</td></tr>
            <tr><td>Spouse Name :</td><td>%s</td></tr>
            <tr><td>Spouse Phone No :</td><td>%s</td></tr>
            <tr><td>Spouse Email :</td><td>%s</td></tr>
            <tr><td>Spouse Date of Birth :</td><td>%s</td></tr>
            <tr><td>Spouse Sin No:</td><td>%s</td></tr>
            <tr><td>Spouse Income:</td><td>%s</td></tr>
            </table>
            <p>Yes, I/We <b>%s, %s</b> Read and Agreed to the Disclosure below.</p>
            <p>DISCLOSURE</p>
            <p>By submitting this application, you authorize Happy Loan Corp., K5 Mortgage Corp., and/or its assignees to obtain reports containing credit and/or personal information as required for the purpose of determining credit worthiness. All information that is obtained is held strictly confidential. Also by submitting your email information you authorize Happy Loan Corp., K5 Mortgage Corp., and/or its assignees to retain your email address for the purpose of communication with you and/or sending informational emails about products, rates and other promotions. You can withdraw your consent at any time. </p>
            </body></html>"""%(
                formatNA(lname),
                formatNA(fname),
                formatNA(fname),
                formatNA(lname),
                formatNA(instance.ph_no),
                formatNA(instance.email_id),
                formatNA(formatCurrency(instance.money)),
                formatNA(instance.whatsfor),
                'YES' if formatNA(instance.ownerhome) else 'NO',
                formatNA(instance.hear_about_us),
                formatNA(instance.referral),
                formatNA(instance.address_line_1),
                formatNA(instance.date_of_birth),
                formatNA(formatCurrency(instance.month_income)),
                formatNA(instance.sin_no),
                formatNA(instance.city),
                formatNA(instance.state),
                formatNA(instance.zip),
                'YES' if instance.other_property else 'NO',
                formatNA(str(instance.work_type)),
                'YES' if instance.are_you_married else 'NO',
                formatNA(instance.spouse_name),
                formatNA(instance.spouse_phone),
                formatNA(instance.spouse_email),
                formatNA(instance.spouce_date_of_birth),
                formatNA(instance.spouce_sin_no),
                formatNA(formatCurrency(instance.spouse_income)),
                formatNA(lname),
                formatNA(fname))

        mail = EmailMessage(sub, data, to=to_email,from_email = settings.EMAIL_FROM)
        mail.content_subtype = "html"
        try:
            mail.send()
            print('mail send for step 2')
        except Exception as e:
            print('mail error',str(e))

    except Exception as e:
        print('error=',str(e))

def sendStepOneMail(instance,created):
    print('In Step One Mail')
    # to = settings.TO_EMAIL
    conf=Configuration.objects.all().last()
    to=list(str(conf.emails).split(','))
    print('to email=',to)
    fname = instance.first_name
    phno=instance.ph_no
    lname = instance.last_name
    emailid=instance.email_id
    referral=instance.referral
    
    if(created):
        sub='New LEAD- '+str(lname).title()+', '+str(fname).title()
   
    else:
        sub="New LEAD- "+ lname+', '+ fname+'(Updated)'    

    if instance.work_type is not None and not created:
        sendmail_for_step2(instance,to)
        return
    
    
    if instance.ownerhome and created:        
        data = """<html><body>
            <b style="font-size:14px">New lead:%s, %s has completed step one</b>
            <table>
            <tr>
            <td>First Name :</td><td>%s</td>
            </tr>
            <tr><td>Last Name :</td><td>%s</td></tr>
            <tr><td>Phone No :</td><td>%s</td></tr>
            <tr><td>Email Id :</td><td>%s</td></tr>
            <tr><td>Money :</td><td>%s</td></tr>
            <tr><td>What is it for :</td><td>%s</td></tr>
            <tr><td>Home owner :</td><td>%s</td></tr>
            <tr><td>Hear About Us :</td><td>%s</td></tr>
            <tr><td>Referral :</td><td>%s</td></tr>
            </table>
            </body></html>"""%(
                formatNA(lname),
                formatNA(fname),
                formatNA(fname),
                formatNA(lname),
                formatNA(phno),
                formatNA(emailid),
                formatNA(formatCurrency(instance.money)),
                formatNA(instance.whatsfor),
                'YES' if formatNA(instance.ownerhome) else 'NO',
                formatNA(instance.hear_about_us),
                formatNA(referral))

        mail = EmailMessage(sub, data, to=to,from_email = settings.EMAIL_FROM)
        mail.content_subtype = "html" 
    # print(created)
        try:
            mail.send()
            print('mail send to admin owner for step 1')
        except Exception as e:
            print('mail error',str(e))
    else:
        
        sub1="Thank you for applying, "+ str(fname).title()
        data = """<html><body>
            <p style="font-size:13.5px">Hello, <b>%s %s</b></p>
            <p>Sorry, but we won’t be able to help you at this time, as we provide loans for homeowners only.</p>
            <p>If you don't own a home, we will have to decline your request.</p>
            <br>
            <p><b>However, our referral partners can definitely help you out:</b></p>
            <b>Apply here:</b>
            <a href='https://www.happyloan.ca/nohouseloan/'><u><span style="color:blue">https://www.happyloan.ca/nohouseloan/</span></u></a>
            <br><hr/>
            <table>
            <tr><td>Happy Loan Corp. Team</td></tr>
            <tr><td>%s</td></tr>
            <tr><td>yes@happyloan.ca</td></tr>
            <tr><td>happyloan.ca</td></tr>
            </table>    
            </body></html>"""%(formatNA(lname),formatNA(fname),formatNA(conf.phone))

        mail = EmailMessage(sub1, data, to=[emailid],from_email = settings.EMAIL_FROM)
        mail.content_subtype = "html"
        try:
            mail.send()
            print('mail send to customer for step 1')
        except Exception as e:
            print('mail error',str(e))
    

        data = """<html><body>
            <line><b>Attention: !!!Lead Don’t own a home!!!</b></line>
            <p><b>New lead:%s,%s has completed step one</b></p>
            <table>
            <tr>
            <td>Fisrt Name :</td><td>%s</td>
            </tr>
            <tr><td>Last Name :</td><td>%s</td></tr>
            <tr><td>Phone No :</td><td>%s</td></tr>
            <tr><td>Email Id :</td><td>%s</td></tr>
            <tr><td>Money :</td><td>%s</td></tr>
            <tr><td>What is it for :</td><td>%s</td></tr>
            <tr><td><b>Home owner :</b></td><td><b>%s</b></td></tr>
            <tr><td>Hear About Us :</td><td>%s</td></tr>
            <tr><td>Referral :</td><td>%s</td></tr>
            </table>
            </body></html>"""%(formatNA(lname),formatNA(fname),formatNA(fname),formatNA(lname),formatNA(phno),formatNA(emailid),formatNA(formatCurrency(instance.money)),formatNA(instance.whatsfor),formatNA(instance.ownerhome),formatNA(instance.hear_about_us),formatNA(referral))
                
                
        mail = EmailMessage(sub, data, to=to,from_email = settings.EMAIL_FROM)
        mail.content_subtype = "html" 
    # print(created)
        try:
            mail.send()
            print('mail send to admin owner for step 1')
        except Exception as e:
            print('mail error',str(e))
        


def sendFinalMails(instance):
    # to = settings.TO_EMAIL


    try:
        createpdf(instance)
    except:
        pass
    # try:
    #     insert_lead_to_zoho(instance)
    # except:
    #     pass
    conf=Configuration.objects.all().last()
    to_emails=list(str(conf.emails).split(','))
    print('to email=',to_emails)

    fname = instance.first_name
    phno=instance.ph_no
    lname = instance.last_name
    emailid=instance.email_id
    referral=instance.referral
    try:
        dob =instance.date_of_birth.strftime("%Y-%m-%d")
    except:
        dob = 'N/A'
    try:
        spouse_dob = instance.spouce_date_of_birth.strftime("%Y-%m-%d")
    except:
        spouse_dob='N/A'    
    # to_arr = []
    # to_arr.append(to)
    try:
        sub="New APPLICATION-"+str(lname).title()+', '+str(fname).title()

        data = """<html><body>
            <p style="font-size:13.5px">New HLC Application:<b>%s, %s</b> has completed full app</p>
            <table>
            <tr>
            <td>First Name :</td><td>%s</td>
            </tr>
            <tr><td>Last Name :</td><td>%s</td></tr>
            <tr><td>Phone No :</td><td>%s</td></tr>
            <tr><td>Email Id :</td><td>%s</td></tr>
            <tr><td>Date of Birth :</td><td>%s</td></tr>
            <tr><td>SIN No :</td><td>%s</td></tr>
            <tr><td>Money :</td><td>%s</td></tr>
            <tr><td>What is it for :</td><td>%s</td></tr>
            <tr><td>Home owner :</td><td>%s</td></tr>
            <tr><td>Hear About Us :</td><td>%s</td></tr>
            <tr><td>Referral :</td><td>%s</td></tr>
            <tr><td>Address :</td><td>%s</td></tr>
            <tr><td>City :</td><td>%s</td></tr>
            <tr><td>State :</td><td>%s</td></tr>
            <tr><td>Postal Code :</td><td>%s</td></tr>
            <tr><td>Other Property :</td><td>%s</td></tr>
            <tr><td>Work Type :</td><td>%s</td></tr>
            <tr><td>Month Income :</td><td>%s</td></tr>
            <tr><td>Are You Married :</td><td>%s</td></tr>
            <tr><td>Spouse Name :</td><td>%s</td></tr>
            <tr><td>Spouse Phone :</td><td>%s</td></tr>
            <tr><td>Spouse Email :</td><td>%s</td></tr>
             <tr><td>Spouse SIN No  :</td><td>%s</td></tr>
            <tr><td>Spouse Date of Birth :</td><td>%s</td></tr>
            <tr><td>Spouse Income :</td><td>%s</td></tr>
            <tr><td> Comments :</td><td>%s</td></tr>
            <tr><td> ID 1 Proof Type :</td><td>%s</td></tr>
            <tr><td> ID 2 Proof Type :</td><td>%s</td></tr>
            <tr><td> Income ID 1 Proof Type :</td><td>%s</td></tr>
            <tr><td> income ID 2 Proof Type :</td><td>%s</td></tr>
            <tr><td> Document Link :</td><td>%s</td></tr>
            </table>
            <p>Yes, I/We <b>%s, %s</b> Read and Agreed to the Disclosure below.</p>
            <p>DISCLOSURE</p>
            <p>By submitting this application, you authorize Happy Loan Corp., K5 Mortgage Corp., and/or its assignees to obtain reports containing credit and/or personal information as required for the purpose of determining credit worthiness. All information that is obtained is held strictly confidential. Also by submitting your email information you authorize Happy Loan Corp., K5 Mortgage Corp., and/or its assignees to retain your email address for the purpose of communication with you and/or sending informational emails about products, rates and other promotions. You can withdraw your consent at any time. </p>
            </body></html>"""%(
                formatNA(lname),
                formatNA(fname),
                formatNA(fname),
                formatNA(lname),
                formatNA(phno),
                formatNA(emailid),
                dob,
                # formatNA(instance.date_of_birth),
                formatNA(instance.sin_no),
                formatNA(formatCurrency(instance.money)),
                formatNA(instance.whatsfor),
                formatNA(instance.ownerhome),
                formatNA(instance.hear_about_us),
                formatNA(referral),
                formatNA(instance.address_line_1),
                formatNA(instance.city),
                formatNA(instance.state),
                formatNA(instance.zip),
                formatNA(instance.other_property),
                formatNA(instance.work_type),
                formatNA(formatCurrency(instance.month_income)),
                formatNA(instance.are_you_married),
                formatNA(instance.spouse_name),
                formatNA(instance.spouse_phone),
                formatNA(instance.spouse_email),
                formatNA(instance.spouce_sin_no),
                spouse_dob,
                # formatNA(instance.spouce_date_of_birth),
                formatNA(formatCurrency(instance.spouse_income)),
                formatNA(instance.comments),formatNA(instance.id1_proof_type),formatNA(instance.id2_proof_type),formatNA(instance.income1_proof_type),formatNA(instance.income2_proof_type),formatNA(settings.HOST_NAME_FOR_MEDIA+instance.id_proof_pdf_file.url),formatNA(lname),formatNA(fname))
                    
        mail = EmailMessage(sub, data, to=to_emails,from_email = settings.EMAIL_FROM)
        mail.content_subtype = "html" 
        
        mail.send()
        print('mail send for step 3')
    except Exception as e:
        print('mail error',str(e))
    

    sub1="Thank you for applying, "+ str(fname).title()            
    data = """<html><body>
        
        <p style="font-size:13.5px">Hello, <b>%s %s</b></p>
        <p>We have received your loan request for %s.</p>
        <p>We will be in touch with you shortly!</p>
        <p>If you have any questions feel free to call/text us at %s or email <u><span style="color:blue">yes@happyloan.ca</span></u></p>
        <br/><hr/>
        <table>
        <tr><td>Happy Loan Corp. Team</td></tr>
        <tr><td>%s</td></tr>
        <tr><td>yes@happyloan.ca</td></tr>
        <tr><td>happyloan.ca</td></tr>
        </table>
        </body></html>"""%(formatNA(fname),formatNA(lname),formatNA(formatCurrency(instance.money)),formatNA(conf.phone),formatNA(conf.phone))
                
    to_arr1=[]
    to_arr1.append(instance.email_id)
    mail = EmailMessage(sub1, data, to=to_arr1,from_email = settings.EMAIL_FROM)
    mail.content_subtype = "html" 
    try:
        mail.send()
        print('mail send of thank you')    
    except Exception as e:
        print('mail error',str(e))

def sendmailandcreatepdf(sender,instance,created, *args, **kwargs):
    print('In Post Save',instance.step_submitted)
  
    if(instance.step_submitted == 'stepone' or instance.step_submitted == 'steptwo'):
        if instance.step_submitted == 'steptwo':
            insert_lead_to_zoho(instance)
        pool = Pool(processes=1)
        pool.apply_async(sendStepOneMail, [instance,created])
        # sendStepOneMail(instance,created)
    elif instance.step_submitted == 'stepfour':
        print("In Step Four")
        insert_lead_to_zoho(instance)
        pool = Pool(processes=1)
        # print('pdf file=',str(instance.id_proof_pdf_file))
        # if len(str(instance.id_proof_pdf_file))<1:
        #     print('call to create pdf')
        #     createpdf(instance)
        # else:
        #     print('call to send mail')c 
        pool.apply_async(sendFinalMails, [instance])

        # sendFinalMails(instance)
    else:
        return
    
    
post_save.connect(sendmailandcreatepdf, sender=PersonDetail)
# post_save.connect(createpdf,sender=PersonDetail)


